$('#contact-question').on('change', function(){
	
	var curr = $(this);
	
	if(curr[0].selectedIndex == 3){
		$('#companion-app-survey').removeClass('hidden');
		$('#inquiry-msg').hide();
	} else{
		$('#companion-app-survey').addClass('hidden');
		$('#inquiry-msg').show()
		$('#contact-message').val("");
	}
});

$('.normalBtn').on('click', function(){
	
	var curr = $(this);
	var isChecked = curr.is(':checked');
	var textBoxId = curr.attr('data-target');
	
	curr
		.parent()
		.parent()
		.find('.otherTextBox')
		.addClass('hidden');
	
	if(textBoxId){
		$(textBoxId).removeClass('hidden');
	}

});

function companionAppData(){
	if($('#contact-question')[0].selectedIndex == 3){
		
		var networkProvider = $('[name="networkProvider"]:checked').attr('value');
		var otherNetworkProvider = $('#networkProvider404');

		var androidVersion = $('[name="androidVersion"]:checked').attr('value');
		var otherAndroidVersion = $('#androidVersion404');
		
		var signalStrength = $('[name="signalStrength"]:checked').attr('value');
		var otherSignalStrength = $('#signalStrength404');
		
		var hadRcmRecomm = $('[name="haveGenerated"]:checked').attr('value');
		var generatedRefId = $('#provideRefId');
		
		var difficultyOnStep = $('[name="difficultOperation"]:checked').attr('value');
		var additionalMessage = $('[name="companion-app-message"]').val();
		
		var companionAppSurveyData = {};
		
		companionAppSurveyData.networkProvider = networkProvider;
		if(otherNetworkProvider.is(':visible')){
			companionAppSurveyData.otherNetworkProvider = otherNetworkProvider.val();
		}

		companionAppSurveyData.androidVersion = androidVersion;
		if(otherAndroidVersion.is(':visible')){
			companionAppSurveyData.otherAndroidVersion = otherAndroidVersion.val();
		}
		
		companionAppSurveyData.signalStrength = signalStrength;
		if(otherSignalStrength.is(':visible')){
			companionAppSurveyData.otherSignalStrength = otherSignalStrength.val();
		}
		
		companionAppSurveyData.hadRcmRecomm = hadRcmRecomm;
		if(generatedRefId.is(':visible')){
			companionAppSurveyData.generatedRefId = generatedRefId.val();
		}
		
		companionAppSurveyData.difficultyOnStep = difficultyOnStep;
		companionAppSurveyData.additionalMessage = additionalMessage;
		
		return (JSON.stringify(companionAppSurveyData));
	}
	return 0;
}
