var appLongName = "Rice Crop Manager", appVersion = "Version 1", appId = "rcm";

function togNav(navs) {
	$(navs).show();
}

function navs(r) {
	switch (r) {
	case 1:
		$('#landing').show();
		$('#aboutHome, #apps, #stats, #contactUs').hide();
		$("#next").show();
		$("#next").val("I agree with the terms and conditions");
		break;
	case 2:
		$('#aboutHome').show();
		$('#landing, #apps, #stats, #contactUs').hide();
		$("#next").hide();
		break;
	case 3:
		$('#apps').show();
		$('#landing, #aboutHome, #stats, #contactUs').hide();
		$("#next").hide();
		break;
	case 4:
		$('#stats').show();
		$('#landing, #aboutHome, #apps, #contactUs').hide();
		$("#next").hide();
		break;
	case 5:
		$('#contactUs').show();
		$('#landing, #aboutHome, #apps,  #stats').hide();
		$("#next").hide();
		break;
	}
	
	if($('.burger').is(':visible')){
		$('.burger').trigger('click');
	}

}

/*function addContent(appLongName, appVersion, appId) {
 var div = document.createElement('div');
 div.innerHTML = '<div id=app_name>'+appLongName+' '+appVersion+' </div><nav id=cssmenu><div class="logo none"><a href=index.html>Menu</a></div><div id=head-mobile></div><div class=burger></div><ul><li><a href=javascript:navs(1);>Home</a><li><a href=javascript:navs(2);>About RCM AS</a><li><a href=#>Apps</a><ul><li id=rcm><a href=http://webapps.irri.org/prod-env1/ph/rcm/ onclick="return window.open(this.href,this.href),!1"target=_blank>Rice Crop Manager (RCM)</a><li id=ffr><a href=http://webapps.irri.org/prod-env1/ph/gpx onclick="return window.open(this.href,this.href),!1"target=_blank>RCM Farmer and Field Registration</a><li id=idm><a href=http://webapps.irri.org/prod-env1/ph/rcmid onclick="return window.open(this.href,this.href),!1"target=_blank>RCM Farmer ID Maker</a><li id=sms><a href=http://webapps.irri.org/prod-env1/ph/rcmsms onclick="return window.open(this.href,this.href),!1"target=_blank>RCM Messenger (Phone call by DA-ATI)</a><li id=sms2><a href=http://webapps.irri.org/prod-env1/ph/rcmsms2 onclick="return window.open(this.href,this.href),!1">RCM SMS Launcher</a><li id=fm><a href=http://webapps.irri.org/prod-env1/ph/rcmfm onclick="return window.open(this.href,this.href),!1"target=_blank>RCM Farming Monitor</a><li id=rvs><a href=http://webapps.irri.org/prod-env1/ph/rcmvs onclick="return window.open(this.href,this.href),!1"target=_blank>RCM Variety Selector</a></ul><li><a href=http://webapps.irri.org/rcma/ onclick="return window.open(this.href,this.href),!1"target=_blank>Statistics</a><li><a href=javascript:navs(5);>Contact us</a></ul></nav>';

 document.getElementById('ras_nav').appendChild(div);

 $('#' + appId).hide();
 }

 window.onload = function(){
 addContent(appLongName, appVersion, appId);
 if(window.location.hash == "#about"){
 navs(2);
 }
 if(window.location.hash == "#contactus"){
 navs(5);
 }
 }*/

(function($) {
	$.fn.menumaker = function(options) {
		var cssmenu = $(this), settings = $.extend({
			format : "dropdown",
			sticky : false
		}, options);
		return this.each(function() {
			$(this).find(".burger").on('click', function() {
				$(this).toggleClass('menu-opened');
				var mainmenu = $(this).next('ul');
				if (mainmenu.hasClass('open')) {
					mainmenu.slideToggle().removeClass('open');
				} else {
					mainmenu.slideToggle().addClass('open');
					if (settings.format === "dropdown") {
						mainmenu.find('ul').show();
					}
				}
			});
			cssmenu.find('li ul').parent().addClass('has-sub');
			multiTg = function() {
				cssmenu.find(".has-sub").prepend(
						'<span class="submenu-button"></span>');
				cssmenu.find('.submenu-button').on(
						'click',
						function() {
							$(this).toggleClass('submenu-opened');
							if ($(this).siblings('ul').hasClass('open')) {
								$(this).siblings('ul').removeClass('open')
										.slideToggle();
							} else {
								$(this).siblings('ul').addClass('open')
										.slideToggle();
							}
						});
			};
			if (settings.format === 'multitoggle')
				multiTg();
			else
				cssmenu.addClass('dropdown');
			if (settings.sticky === true)
				cssmenu.css('position', 'fixed');

			resizeFix = function() {
				var mediasize = 800;
				if ($(window).width() > mediasize) {
					cssmenu.find('ul').show();
				}
				if ($(window).width() <= mediasize) {
					cssmenu.find('ul').hide().removeClass('open');
				}
			};
			resizeFix();
			return $(window).on('resize', resizeFix);
		});
	};
})(jQuery);

(function($) {
	$(document).ready(function() {
		$("#cssmenu").menumaker({
			format : "multitoggle"
		});
		
	$('#contact-cnum')
		.keypad({
			layout: ["123" + $.keypad.CLOSE, "456" + $.keypad.CLEAR,
					 "789" + $.keypad.SPACE + $.keypad.SPACE,
					 "0." + $.keypad.SPACE],
			
			onClose: function(inst) {
				var e = $(this),
					val=e.val();
				
				if ((val.length < 11 && val.length >= 0) || !validatePHMobileNo(val)) {
					$("#warnImg2").attr("src", "../../sa/sl/cross.png").show();
					
				} else {
					$("#warnImg2").attr("src", "../../sa/sl/check.png").show();
				}
			},
			
			onKeypress: function(key, value, inst) { 
								var curr = {}
									curr.fld = $(this);
									curr.id =  curr.fld.attr('id');
									curr.val = curr.fld.val();
									curr.strLen = curr.val.length;
									
								if(curr.strLen == 1){
									if(curr.val != 0){
										curr.fld.val('0'+curr.val)
									}
								}
						    } 
		});		
	});
})(jQuery);
