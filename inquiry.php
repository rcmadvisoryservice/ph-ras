<?php 

class inquiry
{
	public $type;
	public $fname;
	public $lname;
	public $profession;
	public $otherProfession;
	public $email;
	public $cnum;
	public $address;
	public $message;
	public $dbconn;
	
	public $contactType = array(
								1 => 'Inquiry or Question',
								2 => 'Suggestion or Recommendation',
								3 => 'Bug Report'
							);

	
	public $professionType = array(
								1 => 'Agricultural technician',
								2 => 'Farmer',
								3 => 'Fertilizer dealer/seller',
								4 => 'Loan officer',
								5 => 'Researcher',
								6 => 'University professor/instructor',
								7 => 'Student',
								8 => 'Others'
	);
	
	
	public function __construct($get){
		
		$this->type 	= $get['contact-question'];
		$this->fname 	= $get['contact-fname'];
		$this->lname 	= $get['contact-lname'];
		$this->profession		= $get['contact-profession'];
		$this->otherProfession	= $get['contact-other-profession'];
		$this->email 	= $get['contact-email'];
		$this->cnum 	= $get['contact-cnum'];
		$this->address 	= $get['contact-address'];
		$this->message 	= $get['contact-message'];
		
		$this->setDBconn();
		$this->add2DB();
		//$this->send2email('p.sazon@irri.org');
		$this->send2email('r.castillo@irri.org');
		$this->send2email('j.t.torres@irri.org');
	}
	
	public function setDBconn(){
		$mysqli = new mysqli("localhost", "nmriceph", "ASr6Z7Gqe", "mainContactList");
		
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		} else{
			if ($mysqli->set_charset("utf8")) {
				printf("Current character set: %s\n", $mysqli->character_set_name());
			} else {
				printf("Error loading character set utf8: %s\n", $mysqli->error);
			}
	
			$this->dbconn = $mysqli;
		}
	}
	
	public function add2DB(){
		
		$sql = "INSERT INTO `mainContactList`.`phInquiry`
				(`type`,
				`first_name`,
				`last_name`,
				`profession`,
				`other_profession`,
				`email`,
				`contactnumber`,
				`address`,
				`message`,
				`created_at`)
				
				VALUES
				(
				 '".$this->type ."',
				 '".$this->fname."',
				 '".$this->lname."',
				 '".$this->profession."',
				 '".$this->otherProfession."',
				 '".$this->email."',
				 '".$this->cnum."',
				 '".$this->address."',
				 '".$this->message."',
				 '".date("Y-m-d H:i:s")."')";
		
		if ($this->dbconn->query($sql) === TRUE) {
		    echo "New record created successfully";
		} else {
		    echo "Error: " . $sql . "<br>" . $this->dbconn->error;
		}
		
	}
	
	public function send2email($user){
		
		$profession = $this->profession < 8 ? $this->professionType[$this->profession] : ('(Other, specify) '.$this->otherProfession);
		$to = $user;
		$subject = 'Message from RAS homepage';
		$message =  $this->contactType[$this->type]."\n"."\n".
					"Message from: ".$this->fname." ".$this->lname."\n".
					"Profession: ".$profession."\n".
					"Address: ".$this->address."\n".
					"Contact number: ".$this->cnum."\n".
					"Email: ".$this->email."\n".
					"Message: ".$this->message;
		
		$headers = 	"From: appslab@irri.org\r\n";
		$headers .= "MIME-Version: 1.0\r\n"."Content-type: text/plain; charset=iso-8859-1; boundary=\"boundary\"";
		
		
		if(mail($to, $subject, $message, $headers)){
			echo "\n"."Email succeeded to send";
		} else{
			echo "\n"."Email failed to send";
		}
	}
}

$contactus = new inquiry($_GET);
$contactus->dbconn->close();


?>





