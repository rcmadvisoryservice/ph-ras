
var phFolder2 = "ph/ras", 
appName = "RAS PH",
subFolder = "../../", 
rcmLib = "../rcmlib/", 
libFolder = "../../sa/sk/", 
phFolder = "";

function validatePHMobileNo(mobileNo) {
	
	var globe = ['0905','0906','0915','0916','0917','0925','0926','0927','0935','0936','0994','0996','0997','0817','0975','0976','0977','0994','0995','0996','0997'];
	var smart = ['0907','0908','0909','0910','0912','0918','0919','0920','0921','0928','0929','0930','0938','0939','0946','0947','0950','0948','0949','0950','0951','0998','0999'];
	var sun = ['0922','0923','0924','0925','0931','0932','0933','0934','0940','0941','0942','0943','0944'];
	var abs = ['0937'];
	var exetel = ['0973', '0974'];
	var next = ['0977', '0978', '0979'];
	
	
	var all = globe.concat(smart,sun,abs,exetel, next);
	
	var four = mobileNo.substring(0,4);
	
	for (var i=0, l = all.length; i<l; i++) {
		if (four == all[i]) {
			return true;
		}
	}
	
	return false;
}



function validEmail(email) {
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	// console.log(email);
	return re.test(email) ? email : '';
}


$('body').on('keyup','#contact-email', function(){
	
	var e = $(this),
		val=e.val();

	if (val.length == 0 || !validEmail(val)) {
		$("#warnImg1").attr("src", "../../sa/sl/cross.png").show();
		
	} else {
		$("#warnImg1").attr("src", "../../sa/sl/check.png").show();
	}
})


$('body').on('click','#contact-submit', function(){
	var formData = {};
	var contactForm = $('#contact-form').serializeArray();
	for(var fld in contactForm){
		formData[contactForm[fld]['name']] = contactForm[fld]['value'];
	}

	var cemail = $('#contact-email').val();
	var cnum = $('#contact-cnum').val();
	
	var validinputs = ($('#contact-fname').val().length > 0 && $('#contact-lname').val().length > 0);
	var hasProfession = $('#contact-profession').val() < 8 || ($('#contact-profession').val() == 8 && $('#contact-other-profession').val().length > 3 )
	var invalidEmail = (cemail.length == 0 || !validEmail(cemail));
	var invalidMobile = ((cnum.length < 11 && cnum.length >= 0) || !validatePHMobileNo(cnum));
	
	if(validinputs && hasProfession && (!invalidEmail && !invalidMobile) ) {
		var request = $.ajax({
			url: "inquiry.php",
			data: formData
		});


		request.done(function( msg ) {
			$('#contact-form')[0].reset();
			$('#contact-other-profession').removeAttr('required');
			$('#warnImg1,#warnImg2, #other-profession-container').hide();
			console.log( msg );
		});
			 
		request.fail(function( jqXHR, textStatus ) {
			console.log( "Request failed: " + textStatus );
		});
	} else{
		alert('Please answer the form appropriately.');
	}

});

$('body').on('keyup','#contact-fname, #contact-lname, #contact-address, #contact-other-profession', function(){
	if($(this).val().trim()==''){ $(this).attr('value',''); } // remove input if is only spaces

	if (this.value != this.value.replace(/^(.)|\s+(.)/g, function($1){ return $1.toUpperCase(); })){
	   this.value = this.value.replace(/^(.)|\s+(.)/g, function($1){ return $1.toUpperCase(); });
	}
	if (this.value != this.value.replace (/[ ,\t]{2,}/gi,"")){
		this.value = this.value.replace (/[ ,\t]{2,}/gi,""); // replaces multiple spaces with one space
	}
});

$('body').on('change','#contact-profession', function(){
	var curr = $(this);
	
	$('#other-profession-container').toggle(curr.find(':selected').val() == 8).promise().done(function(){
		if(curr.find(':selected').val() == 8){
			$('#contact-other-profession').attr({'required':'true'});
		} 
		
		if(curr.find(':selected').val() < 8){
			$('#contact-other-profession').removeAttr('required');
		}
	});
});