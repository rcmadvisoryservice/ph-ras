<?php
header ( 'Content-Type: text/cache-manifest' );
header ( "Cache-Control: no-cache, must-revalidate" );
?>
CACHE MANIFEST
# This is a comment 
# version 2.02.01
<?php
$modifications = "";
$str = "";

$dir = new RecursiveDirectoryIterator ( "../../sa" );
foreach ( new RecursiveIteratorIterator ( $dir ) as $file ) {
	if ($file->IsFile () && $file != "./offline.appcache" && ! strpos ( $file, ".php" ) && ! strpos ( $file, ".html" ) && substr ( $file->getFilename (), 0, 1 ) != ".") {
		$str .= $file . "\n";
		$modifications .= ' ' . filemtime ( $file );
	}
}

$dir = new RecursiveDirectoryIterator ( "../rcmlib" );
foreach ( new RecursiveIteratorIterator ( $dir ) as $file ) {
	if ($file->IsFile () && $file != "./offline.appcache" && ! strpos ( $file, ".php" ) && ! strpos ( $file, ".html" ) && substr ( $file->getFilename (), 0, 1 ) != ".") {
		$str .= $file . "\n";
		$modifications .= ' ' . filemtime ( $file );
	}
}

$dir = new RecursiveDirectoryIterator ( "." );
foreach ( new RecursiveIteratorIterator ( $dir ) as $file ) {
	if ($file->IsFile () && $file != "./offline.appcache" && ! strpos ( $file, ".php" ) && ! strpos ( $file, ".pdf" ) && substr ( $file->getFilename (), 0, 1 ) != ".") {
		$str .= $file . "\n";
		$modifications .= ' ' . filemtime ( $file );
	}
}

$modifications = sha1 ( $modifications );

echo '#version:' . $modifications . "\n";

?>

CACHE: 
<?php echo $str?>
../../sa/st/index.css

NETWORK:
*
