<?php 

$conn = new mysqli("localhost", "nmriceph", "ASr6Z7Gqe", "mainContactList");
require_once('../../PHPExcel.php');
include_once('../../PHPExcel/Writer/Excel2007.php');

/* check connection */
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setTitle("RAS PH CONTACT US Extraction");
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setSize('8');
$objPHPExcel->getActiveSheet()->getStyle('A1:KX5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('A1:KX5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

$sql = 'SELECT 
			CASE
				WHEN `phInquiry`.`type` = 1 THEN "Inquiry or Question"
				WHEN `phInquiry`.`type` = 2 THEN "Suggestion or Recommendation"
				WHEN `phInquiry`.`type` = 3 THEN "Bug Report"
				ELSE `phInquiry`.`type`
			END as "Feedback type",
			`phInquiry`.`first_name` as "First name", 
			`phInquiry`.`last_name` as "Last name", 
			CASE
				WHEN `phInquiry`.`profession` = 1 THEN "Agricultural technician"
				WHEN `phInquiry`.`profession` = 2 THEN "Farmer"
				WHEN `phInquiry`.`profession` = 3 THEN "Fertilizer dealer/seller"
				WHEN `phInquiry`.`profession` = 4 THEN "Loan officer"
				WHEN `phInquiry`.`profession` = 5 THEN "Researcher"
				WHEN `phInquiry`.`profession` = 6 THEN "University professor/instructor"
				WHEN `phInquiry`.`profession` = 7 THEN "Student"
				WHEN `phInquiry`.`profession` = 8 THEN "Others, please specify:"
				ELSE `phInquiry`.`profession`
			END as "profession", 
			`phInquiry`.`other_profession` as "Specified profession", 
			`phInquiry`.`email` as "Email", 
			`phInquiry`.`contactnumber` as "Contact number",
			`phInquiry`.`address` as "Address", 
			`phInquiry`.`message` as "Message",
			`phInquiry`.`created_at` as "Date submitted"
		 FROM mainContactList.phInquiry
		 WHERE `phInquiry`.`first_name` NOT LIKE "%test%"
			AND `phInquiry`.`last_name` NOT LIKE "%test%"
			AND `phInquiry`.`other_profession` IS NULL
			OR `phInquiry`.`other_profession` NOT LIKE "%test%"
			AND `phInquiry`.`email` NOT LIKE "%test%"
			AND `phInquiry`.`message` NOT LIKE "%test%"';

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	
	/* Get field information for all columns */
	$finfo = $result->fetch_fields();
	$fcount = $result->field_count;
	
	$cells = range('A', 'Z');
	
	
	$cellCol = 0;
	foreach ($finfo as $val) {
		$objPHPExcel->getActiveSheet()->SetCellValue($cells[$cellCol]."1", $val->name);
		if($cellCol < $fcount) $cellCol++;
	}
	
	
	// output data of each row
	$cellRow = 2;
	while($row = $result->fetch_assoc()) {

		$cellCol=0;
		foreach ($finfo as $val) {
			$objPHPExcel->getActiveSheet()->SetCellValue($cells[$cellCol].$cellRow, $row[$val->name]);
			if($cellCol < $fcount) $cellCol++;
		}	
		$cellRow++;
	}
	
} else {
	echo "0 results";
}

$conn->close();

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="RAS PH CONTACT US Extraction.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>