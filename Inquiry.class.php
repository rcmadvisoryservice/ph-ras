<?php

class Inquiry
{
	
	public $debug_mode=false;
    
	public $type;
	public $appAssignment;
	
    public $fname;
    public $lname;
    public $profession;
    public $otherProfession;
    public $email;
    public $cnum;
    public $address;
    public $message;
    public $fileAttachment;
    public $dbconn;
    public $response;
    
    
    public $contactType = array(
        1 => 'Inquiry or Question',
        2 => 'Suggestion or Recommendation',
        3 => 'Bug Report'
    );
    
    
    public $professionType = array(
        1 => 'Agricultural technician',
        2 => 'Farmer',
        3 => 'Fertilizer dealer/seller',
        4 => 'Loan officer',
        5 => 'Researcher',
        6 => 'University professor/instructor',
        7 => 'Student',
        8 => 'Others'
    );
    
    
    public function __construct($get, $file){

    	
    	$serverType = "!AWS";
    	
    	$awslocation = "/var/app/current/ph/contact_us_user_uploads/";
    	$legacyLocation = "/var/www/webapps/ph/contact_us_user_uploads/";
    	
    	$serverLocation = $serverType=="AWS"?$awslocation:$legacyLocation;
    	
    	$awsURL = $this->debug_mode?"https://phapps-dev.irri.org/ph/contact_us_user_uploads/":"https://phapps.irri.org/ph/contact_us_user_uploads/";
    	$legacyURL = $this->debug_mode?"http://appslab.irri.org/ph/contact_us_user_uploads/":"http://webapps.irri.org/ph/contact_us_user_uploads/";
    	
    	$serverURL = $serverType=="AWS"?$awsURL:$legacyURL;
    	
    	$timestamp = date("Y-m-d H:i:s");
    	// ----------------------------------------------------------------------
    	
    	$mysqli_object = new Connection();
    	$this->dbconn = $mysqli_object->getConnection();
    	
    	$this->type 	= $this->post_number_int('contact-question');
    	$this->appAssignment = $this->post_string_val('contact-for-app');
        
    	$this->fname 	= $this->post_string_val('contact-fname');
    	$this->lname 	= $this->post_string_val('contact-lname');
    	$this->profession		= $this->post_number_int('contact-profession');
    	$this->otherProfession	= $this->post_string_val('contact-other-profession');
    	$this->email 	= $this->post_string_val('contact-email');
    	$this->cnum 	= $this->post_string_val('contact-cnum');
    	$this->address 	= $this->post_string_val('contact-address');
    	$this->message 	= $this->post_string_val('contact-message');
        
        $file_tmp = $file['attached_file']['tmp_name'];
        $fileType = $file['attached_file']['type'];
        $isNotImgFile = ($fileType != 'image/jpeg' && $fileType != 'image/jpg' && $fileType != 'image/png');
        
        $isNotGPXFile = ($fileType != "application/octet-stream" && $fileType != "text/xml");

        $isNotMSDocFile = ($fileType != "text/plain" && 
        				   $fileType != "application/msword" && 
        				   $fileType != "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        
        $isNotMSExcelFile = ($fileType != "text/csv" && 
        					 $fileType != "application/vnd.ms-excel" && 
        					 $fileType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        
        $notValidType = ($isNotImgFile && $isNotGPXFile && $isNotMSDocFile && $isNotMSExcelFile);

        $file_size = $this->byteConvert($file['attached_file']['size']);
        $file_size_unit = $file_size['unit'];
        $file_size_amount = $file_size['amount'];
        
        
        $notValidFileUnit = ($file_size_unit != "KB" && $file_size_unit != "MB");
        $notValidKB = ($file_size_unit == "KB" && ($file_size_amount < 1 || $file_size_amount > 1024.1));
        $notValidMB = ($file_size_unit == "MB" && ($file_size_amount < 1 || $file_size_amount > 5));
  
        if($this->debug_mode){
        	
        	$this->response = 204; // debug mode
        	
        } else {
        	
        	if( !empty($file_tmp) && is_uploaded_file( $file_tmp)){
	
		       if($notValidType || ($notValidKB && $notValidMB)){
	        		
		        	$this->message .= ("<br/> <b>Error 413:</b> Unmet requirements. <br/>". 
									  "Invalid file type: ".$fileType." or<br/>".
		        					  "Invalid file size: ".$file_size_amount.$file_size_unit);
					$this->send2email('e.banasihan@irri.org');
		        	$this->send2email('p.sazon@irri.org');
		        	$this->response = 413;
		        	return;
		        	
	     	   } else{
	         	
	        		$filename = $filesize = $fileloc = $thumb_name = '';
	
	        		$file_name = $file['attached_file']['name'];
	        		
	        		$extract = explode(".", $file_name);
	        		$file_ext = $extract[count($extract)-1];
	        		
	        		
	        		$new_fileName = strtotime($timestamp).".".$file_ext;
	        		$upload_location = $serverLocation.$new_fileName;
	        		
	        		if (move_uploaded_file($file_tmp, $upload_location)) {
	
	        			$this->fileAttachment = $upload_location;
	        			if($this->appAssignment != "Rice Crop Manager Companion App"){
	        				$this->message .= '<br/> File attachement: <a href="'.$serverURL.$new_fileName.'" alt="'.$upload_location.'" target="_blank">'.
	          				$serverURL.$new_fileName.
	          				'</a>';
	        			}
	
	        		} else{
	        			$this->message .= ('<br/> <b>Error 413:</b> Temporary file : '.json_encode(error_get_last())."\n");
						$this->send2email('e.banasihan@irri.org');
	        			$this->send2email('p.sazon@irri.org');
	        			$this->response = 413;
	        			return;
	        		}
	        		
	       		}
	        }

			$inserted2Database = $this->add2DB();
			if($inserted2Database){
        		
        		try {
        			
        			if($debug_mode == false && $this->type < 4){
        				$this->send2email('e.banasihan@irri.org');
        				$this->send2email('r.castillo@irri.org');
        				$this->send2email('j.t.torres@irri.org');
        				$this->send2email('n.averion@irri.org');
        				$this->send2email('a.baradas@irri.org');
        				$this->send2email('b.jardinero@irri.org');
        			}
        			$this->response = 200; // status code for true positive success
        			
        		} catch (Exception $e) {

        			$this->message .= "<br/> <b>Error 417:</b> Sending contact us message to email is not working (catch message : ".json_encode($e)."). Please Check ASAP! :(";
        			$this->send2email('e.banasihan@irri.org');
        			$this->send2email('p.sazon@irri.org');
        			$this->response = 417; // The request has been stored to database, but the email has not been sent.
        			
        		}
        		
        		
        	} else{
        		
        		$this->message .= "<br/> <b>Error 420:</b> Storing and Sending contact us message is not working. Please Check ASAP! :(";
        		$this->send2email('e.banasihan@irri.org');
        		$this->send2email('p.sazon@irri.org');
        		$this->response = 420; // The request is not stored to database, but the email has not been sent. 
        		
        	}
        }
        
    }
    
    public function add2DB(){
        
        $sql = "INSERT INTO `mainContactList`.`phInquiry`
				(`type`,
				`app_assingment`,
				`first_name`,
				`last_name`,
				`profession`,
				`other_profession`,
				`email`,
				`contactnumber`,
				`address`,
				`message`,
				`file_attachment`,
				`created_at`)
            
				VALUES
				(
				 '".$this->type."',
				 '".$this->appAssignment."',
				 '".$this->fname."',
				 '".$this->lname."',
				 '".$this->profession."',
				 '".$this->otherProfession."',
				 '".$this->email."',
				 '".$this->cnum."',
				 '".$this->address."',
				 '".$this->message."',
				 '".$this->fileAttachment."',
				 '".date("Y-m-d H:i:s")."')";
        
        if ($this->dbconn->query($sql) === TRUE) {
        	#echo "New record created successfully";
        	$returnVal = 1;
        } else {
        	#echo "Error: " . $sql . "<br>" . $this->dbconn->error;
        	$this->message .= "<br/><br/><b>Database Error:</b> SQL statement: ".$sql."<br/> *Returned error: ".json_encode($this->dbconn->error);
        	$returnVal = 0;
        }
        $this->dbconn->close();
        return $returnVal;
    }
    
    public function send2email($user){

        $mailer = new AppsLabMailer();
        
        $profession = $this->profession < 8 ? $this->professionType[$this->profession] : ('(Other, specify) '.$this->otherProfession);
        $to = $user;
        $subject = 'Message from RAS homepage';
        $message =  "<b>".$this->contactType[$this->type]."</b> - <b>".$this->appAssignment."</b><br/><br/>".
            "Message from: ".$this->fname." ".$this->lname."<br/>".
            "Profession: ".$profession."<br/>".
            "Address: ".$this->address."<br/>".
            "Contact number: ".$this->cnum."<br/>".
            "Email: ".$this->email."<br/>".
            "Message: ".$this->message."<br/>";
        
        try{
        	
        	// $from, $fromAlias, $to, $toAlias, $replyTo, $replyToAlias, $subject, $body, $altBody
        	$emailSent = $mailer->sendMail(null, null, $to, "RCM-Inquiry", null, null, $subject, $message, null);
        	
        	if($emailSent){
        		// echo "\n"."Email sent";
        		return 1;
        	} else{
        		// echo "\n"."Failed to send email";
        		return 0;
        	}

        }catch (Exception $e) {
        	
        	$errorMessage .= "<br/><br/><b>Error Sending to email:</b> Recipient: ".$to.". With error message: ".json_encode($e);
        	$mailer->sendMail(null, 
        					null, 
        					"e.banasihan@irri.org", 
        					"RCM-RAS-contactus-failed", 
        					null, 
        					null, 
        					"RCM-RAS-contactus-failed", 
        					$errorMessage, 
        					null);
        	
        	$mailer->sendMail(null, 
        					null,
        					"p.sazon@irri.org",
        					"RCM-RAS-contactus-failed",
        					null, 
        					null,
        					"RCM-RAS-contactus-failed",
        					$errorMessage, 
        					null);

        	return 0;
        }


    }
    
    // https://stackoverflow.com/questions/15188033/human-readable-file-size
    public function byteConvert($bytes)
    {
    	if ($bytes == 0)
    		return "0.00 B";
    		
    		$s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    		$e = floor(log($bytes, 1024));
    		
    		return array("amount"=>round($bytes/pow(1024, $e), 2), "unit"=>$s[$e]);
    }
    
    
   	public static function post_use_default($name){
   		$input = filter_input(INPUT_POST, $name, FILTER_DEFAULT);
   		$input = isset($input)?$input:null;
   		$input = $input==null?0:$input;
   		return $input;
   	}
   	
   	public static function post_number_int($name){
   		$input = filter_input(INPUT_POST, $name, FILTER_SANITIZE_NUMBER_INT);
   		$input = isset($input)?$input:null;
   		$input = $input==null?0:$input;
   		return $input;
   	}
    	
   	public static function post_string_val($name){
  		$input = filter_input(INPUT_POST, $name, FILTER_SANITIZE_STRING);
   		$input = isset($input)?$input:null;
   		$input = $input==null?0:$input;
   		return $input;
   	}
    	
   	public static function post_string_email($name){
   		$input = filter_input(INPUT_POST, $name, FILTER_SANITIZE_EMAIL);
   		$input = isset($input)?$input:null;
   		$input = $input==null?0:$input;
   		return $input;
   	}
    	
    	
   	public static function post_number_float($name){
   		$input = filter_input(INPUT_POST, $name, FILTER_VALIDATE_FLOAT);
   		$input = isset($input)?$input:null;
   		$input = $input==null?0:$input;
   		return $input;
   	}
    	
   	public static function post_boolean($name){
   		$input = filter_input(INPUT_POST, $name, FILTER_VALIDATE_BOOLEAN);
   		$input = isset($input)?$input:null;
   		$input = $input==null?0:$input;
   		return $input;
    }
}


?>





