<?php 

class Connection
{
    public $host = "localhost";
    public $user = "nmriceph";
    public $password = "ASr6Z7Gqe";
    public $database = "mainContactList";
    public $openConnection;
    
    
    public function __construct($property = array()){
        
        $this->host = isset($property['host']) ? $property['host'] : $this->host;
        $this->user = isset($property['user']) ? $property['host'] : $this->user;
        $this->password = isset($property['password']) ? $property['password'] : $this->password;
        $this->database = isset($property['database']) ? $property['database'] : $this->database;
        
        $this->setConnection();
    }
    
    public function setConnection(){
        
        $mysqli = new mysqli($this->host, $this->user, $this->password, $this->database);
        
        if ($mysqli->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
            $this->openConnection = $mysqli->connect_error;
        } else{
            if ($mysqli->set_charset("utf8")) {
                # printf("Current character set: %s\n", $conn->character_set_name());
            } else {
                # printf("Error loading character set utf8: %s\n", $conn->error);
                die();
            }
            
            $this->openConnection = $mysqli;
        }
    }
    
    public function getConnection(){
        return $this->openConnection;
    }
    
    
    
}

?>